const token = '63896-88a2aa76-15ef-4a90-a63c-0ff3a38d30aa';
const base = 'https://api.figma.com'
const endpoint = '/v1/files/'
const docId = 'RtCydRnCfFXej446gsC2E0';
const buttonId = '493:6356'

async function getData(id, button){
    const res = await fetch(base+endpoint+id+'/nodes?ids='+button, {
        method: 'GET',
        headers: {
            'X-Figma-Token': token
        }
    })
        .then(response=>{
            return response.json()
        })
        .then(tree=> {
            console.log(tree);
            const buttonData = tree.nodes[buttonId].document;
            const styles = recurseSearch(buttonData);
/*          styles.width = buttonData[0].absoluteBoundingBox.width;
            styles.height = buttonData[0].absoluteBoundingBox.height;
            styles.background = buttonData[0].children[0].fills[0].color;
            styles['border-radius'] = buttonData[0].children[0].rectangleCornerRadii;
            styles['border-color'] = buttonData[0].children[0].strokes[0].color;
            styles['border-type'] = buttonData[0].children[0].strokes[0].type;
            styles['border-width'] = buttonData[0].children[0].strokeWeight; //Не уверен, что именно это свойство отвечает за ширину
            styles['font-family'] = buttonData[1].style.fontFamily;
            styles['font-size'] = buttonData[1].style.fontSize;
            styles['font-weight'] = buttonData[1].style.fontWeight;
            styles['letter-spacing'] = buttonData[1].style.letterSpacing;
            styles['line-height'] = buttonData[1].style.lineHeightPx;
            styles['text-align'] = buttonData[1].style.textAlignHorizontal;
            styles.color = buttonData[1].fills[0].color;*/
            const jsonStr = JSON.stringify(styles);
            download(jsonStr, 'config');
            document.querySelector('.output').innerText = jsonStr;
            console.log(styles);
        })
}

function recurseSearch(elem){
    const styles = {
        width: null,
        height: null,
        'border-radius': null,
        'border-color': null,
        'border-type': null,
        'border-width': null,
        background: null,
        color: null,
        'font-family': null,
        'font-size': null,
        'font-weight': null,
        'letter-spacing':null,
        'line-height': null,
        'text-align': null
    };

    function dive(elem){
        if (elem.type !== "RECTANGLE" && elem.type !== "TEXT" && elem.children){
            for (let i = 0; i < elem.children.length; i+=1){+
                dive(elem.children[i])
            }
        }
        else if (elem.type === "RECTANGLE"){
            styles.width = elem.absoluteBoundingBox.width;
            styles.height = elem.absoluteBoundingBox.height;
            styles.background = elem.fills[0].color;
            styles['border-radius'] = elem.rectangleCornerRadii;
            styles['border-color'] = elem.strokes[0].color;
            styles['border-type'] = elem.strokes[0].type.toLowerCase();
            styles['border-width'] = elem.strokeWeight;
        }
        else if (elem.type === "TEXT"){
            styles['font-family'] = elem.style.fontFamily;
            styles['font-size'] = elem.style.fontSize;
            styles['font-weight'] = elem.style.fontWeight;
            styles['letter-spacing'] = elem.style.letterSpacing;
            styles['line-height'] = elem.style.lineHeightPx;
            styles['text-align'] = elem.style.textAlignHorizontal.toLowerCase();
            styles.color = elem.fills[0].color;
        }

        return styles
    }

    return dive(elem)
}

function download(exportObj, exportName){
    const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(exportObj);
    const link = document.querySelector('.downloadLink');
    link.setAttribute("href",     dataStr);
    link.setAttribute("download", exportName + ".json");
    link.click();
}

getData(docId, buttonId);